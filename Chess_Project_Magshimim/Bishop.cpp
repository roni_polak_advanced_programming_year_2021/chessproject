#include "Bishop.h"

//sets fields
Bishop::Bishop(const bool color, const string& loc, Board& board) : Piece('B', loc, color, board)
{

}

bool Bishop::isMovable(const string& newLocation)
{
	bool error = false;

	pair<int, int> location = getBoardLocation(newLocation);

	if (abs(_numberLocation - location.second) == abs(_letterLocation - location.first))//if it goes diagonal
	{
		if (location.second > _numberLocation && location.first > _letterLocation)//down right diagonal
		{
			for (int i = location.first - 1, j = location.second - 1; i > _letterLocation && j > _numberLocation; i--, j--)//goes through diagonal from the destination location to the current location
			{
				if (_board._board[j][i] != NULL)//checks if there is a piece in the middle
				{
					error = true;
					break;
				}
			}
		}
		if (location.second < _numberLocation && location.first > _letterLocation)//up right diagonal
		{
			for (int i = location.first - 1, j = location.second + 1; i > _letterLocation && j < _numberLocation; i--, j++)//goes through diagonal from the destination location to the current location
			{
				if (_board._board[j][i] != NULL)//checks if there is a piece in the middle
				{
					error = true;
					break;
				}
			}
		}
		if (location.second > _numberLocation && location.first < _letterLocation)//down left diagonal
		{
			for (int i = location.first + 1, j = location.second - 1; i < _letterLocation && j > _numberLocation; i++, j--)//goes through diagonal from the destination location to the current location
			{
				if (_board._board[j][i] != NULL)//checks if there is a piece in the middle
				{
					error = true;
					break;
				}
			}
		}
		if (location.second < _numberLocation && location.first < _letterLocation)//up left diagnoal
		{
			for (int i = location.first + 1, j = location.second + 1; i < _letterLocation && j < _numberLocation; i++, j++)//goes through diagonal from the destination location to the current location
			{
				if (_board._board[j][i] != NULL)//checks if there is a piece in the middle
				{
					error = true;
					break;
				}
			}
		}
	}
	else//not diagonal
	{
		error = true;
	}

	return !error;//returns if piece can move
}