#pragma once
#include "Piece.h"
#include <iostream>

using std::string;

class Bishop : virtual public Piece
{
public:
	Bishop(const bool color, const string& loc, Board& board);//constructor
	virtual bool isMovable(const string& newLocation);//checks if bishop can move to a certain location
};