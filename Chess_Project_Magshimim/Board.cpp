#include "Knight.h"
#include "Queen.h"
#include "Board.h"
#include "Rook.h"
#include "King.h"
#include "Pawn.h"
#include "Bishop.h"

#define GET_LETTER_ASCII 97
#define GET_NUM_ASCII 49
#define BOARD_RESET 7

Board::Board()
{
	for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			_board[i][j] = NULL;
		}
	}
	//creates pieces in board
	_board[0][0] = new Rook(false, "a8", *this);
	_board[0][7] = new Rook(false, "h8", *this);
	_board[7][0] = new Rook(true, "a1", *this);
	_board[7][7] = new Rook(true, "h1", *this);

	_board[0][2] = new Bishop(false, "c8", *this);
	_board[0][5] = new Bishop(false, "f8", *this);
	_board[7][2] = new Bishop(true, "c1", *this);
	_board[7][5] = new Bishop(true, "f1", *this);

	_board[0][3] = new King(false, "d8", *this);
	_blackKingLocation = "d8";
	_board[7][3] = new King(true, "d1", *this);
	_whiteKingLocation = "d1";

	_board[1][0] = new Pawn(false, "a7", *this);
	_board[1][1] = new Pawn(false, "b7", *this);
	_board[1][2] = new Pawn(false, "c7", *this);
	_board[1][3] = new Pawn(false, "d7", *this);
	_board[1][4] = new Pawn(false, "e7", *this);
	_board[1][5] = new Pawn(false, "f7", *this);
	_board[1][6] = new Pawn(false, "g7", *this);
	_board[1][7] = new Pawn(false, "h7", *this);

	_board[6][0] = new Pawn(true, "a2", *this);
	_board[6][1] = new Pawn(true, "b2", *this);
	_board[6][2] = new Pawn(true, "c2", *this);
	_board[6][3] = new Pawn(true, "d2", *this);
	_board[6][4] = new Pawn(true, "e2", *this);
	_board[6][5] = new Pawn(true, "f2", *this);
	_board[6][6] = new Pawn(true, "g2", *this);
	_board[6][7] = new Pawn(true, "h2", *this);
	
	_board[0][4] = new Queen(false, "e8", *this);
	_board[7][4] = new Queen(true, "e1", *this);

	_board[0][1] = new Knight(false, "b8", *this);
	_board[0][6] = new Knight(false, "g8", *this);
	_board[7][1] = new Knight(true, "b1", *this);
	_board[7][6] = new Knight(true, "g1", *this);

}

void Board::move(const string& oldLocation, const string& newLocation)
{
	const unsigned int newLetter = (int(newLocation[0]) - GET_LETTER_ASCII);
	const unsigned int newNumber = BOARD_RESET - (int(newLocation[1]) - GET_NUM_ASCII);

	const unsigned int currLetter = (int(oldLocation[0]) - GET_LETTER_ASCII);
	const unsigned int currNumber = BOARD_RESET - (int(oldLocation[1]) - GET_NUM_ASCII);

	_board[newNumber][newLetter] = _board[currNumber][currLetter];
	_board[currNumber][currLetter] = NULL;
}