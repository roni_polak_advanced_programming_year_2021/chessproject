#pragma once

#include <string>
#include <iostream>
#include "Piece.h"

using namespace std;

class Piece;
class Board
{

public:

	/*
	* a8->d3
	* 0,0->5,3
	
	8  R # # K # # # R
	7  # # # # # # # #
	6  # # # # # # # #
	5  # # # # # # # #
	4  # # # # # # # #
	3  # # # # # # # #
	2  # # # # # # # #
	1  r # # k # # # r
	   a b c d e f g h
	
	*/

	Board();
	void move(const string& oldLocation, const string& newLocation);//the move assums to be legal

	Piece* _board[8][8];//contains all the pieces in the board (pointers to pieces)


	//contains the kings location in the board at all times
	string _whiteKingLocation;
	string _blackKingLocation;

};

