#include "King.h"

King::King(const bool color, const string& loc, Board& board) : Piece('K', loc, color, board) {  }

/*
This code is may be complicated to read. It checks that the king's move is a valid one from the number of squares espect:

 #  #  #  #  #  #  #  # 
 #  #  #  #  #  #  #  #
 #  #  @  @  @  #  #  # 
 #  #  @  K  @  #  #  #
 #  #  @  @  @  #  #  # 
 #  #  #  #  #  #  #  #
 #  #  #  #  #  #  #  # 
 #  #  #  #  #  #  #  #  , 

 #  #  #  #  #  #  #  #
 #  #  #  #  #  #  #  #
 #  #  #  #  #  #  #  #
 #  #  #  #  #  #  #  #
 #  #  #  #  #  #  #  #
 #  #  #  #  #  #  #  #
 #  #  #  #  #  #  @  @
 #  #  #  #  #  #  @  K

 This function make sure the king is moving to a valid place (the places mark with '@'): Return 'false' if not...

 */
bool King::isMovable(const string& newLocation)
{
	pair<int,int> location = getBoardLocation(newLocation);

	if ((abs(_numberLocation - location.second) == 1 && ((abs(_letterLocation - location.first) <= 1)) || (abs(_letterLocation - location.first) == 1) && (abs(_numberLocation - location.second) <= 1)))//checks if king moves 1 unit
	{
		//cahnges king location in board according to the king's color
		if (this->_color == true)
		{
			_board._whiteKingLocation = newLocation;
		}
		else
		{
			_board._blackKingLocation = newLocation;
		}
		return true;
	}
	else
	{
		return false;
	}
}