#pragma once
#include "Piece.h"
#include <iostream>

using std::string;

class King : public Piece
{
public:
	King(const bool color, const string& loc, Board& board);//constructor
	virtual bool isMovable(const string& newLocation);//checks if king can move to a certain location
};