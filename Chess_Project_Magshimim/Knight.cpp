#include "Knight.h"

Knight::Knight(const bool color, const string& loc, Board& board) : Piece('N', loc, color, board) {  }

bool Knight::isMovable(const string& newLocation)
{
	pair<int, int> location = getBoardLocation(newLocation);

	//returns if the knight can move to the given location
	return ((abs(_numberLocation - location.second) == 2 && ((abs(_letterLocation - location.first) == 1)) || (abs(_letterLocation - location.first) == 2) && (abs(_numberLocation - location.second) == 1)));
}
