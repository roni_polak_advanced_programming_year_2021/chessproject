#pragma once
#include "Piece.h"
#include <iostream>

using std::string;

class Knight : public Piece
{
public:
	Knight(const bool color, const string& loc, Board& board);//constructor
	virtual bool isMovable(const string& newLocation);//checks if knight can move to a certain location
};