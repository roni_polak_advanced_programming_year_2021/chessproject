#include "Pawn.h"

Pawn::Pawn(const bool color, const string& loc, Board& board) : Piece('P', loc, color, board)
{
	isFirstTurn = true;
}

bool Pawn::isMovable(const string& newLocation)
{
	pair<int, int> location = getBoardLocation(newLocation);

	if (this->getColor() == false)//black
	{
		if (this->isFirstTurn)//is first turn of the pawn
		{
			if (location.second == (_numberLocation + 2) && location.first == _letterLocation && _board._board[location.second][location.first] == NULL)//if move two
			{
				isFirstTurn = false;
				return true;
			}
			else
			{
				if (location.second == (_numberLocation + 1))//if move one forward
				{
					if (location.first == _letterLocation)//if move one forward
					{
						if (_board._board[_numberLocation + 1][_letterLocation] == NULL)//if empty space
						{
							isFirstTurn = false;
							return true;
						}
						else
						{
							return false;
						}
					}
					else if (location.first == _letterLocation - 1 || location.first == _letterLocation + 1)//if move diagnol
					{
						if (_board._board[location.second][location.first] != NULL && _board._board[location.second][location.first]->getColor() != false)//if not empty and enemy in square
						{
							isFirstTurn = false;
							return true;
						}
						else
						{
							return false;
						}
					}
					else
					{
						return false;
					}


				}
				else
				{
					return false;
				}
			}
		}
		else//not first turn of pawn
		{
			if (location.second == (_numberLocation + 1))//if move forward one
			{
				if (location.first == _letterLocation)//if move forward one
				{
					if (_board._board[_numberLocation + 1][_letterLocation] == NULL)//if empty square
					{
						isFirstTurn = false;
						return true;
					}
					else
					{
						return false;
					}
				}
				else if (location.first == _letterLocation - 1 || location.first == _letterLocation + 1)//if diagnol take
				{
					if (_board._board[location.second][location.first] != NULL && _board._board[location.second][location.first]->getColor() != false)//if not empty and enemy color
					{
						isFirstTurn = false;
						return true;
					}
					else
					{
						return false;
					}
				}
				else
				{
					return false;
				}

			}
			else
			{
				return false;
			}
		}
	}
	else//if white
	{

		if (this->isFirstTurn)//is first turn of the pawn
		{
			if (location.second == (_numberLocation - 2) && location.first == _letterLocation && _board._board[location.second][location.first] == NULL)//if move two forward
			{
				isFirstTurn = false;
				return true;
			}
			else//not move two forward
			{
				if (location.second == (_numberLocation - 1))//if move one forward
				{
					if (location.first == _letterLocation)//if move one forward
					{
						if (_board._board[_numberLocation - 1][_letterLocation] == NULL)//if empty
						{
							isFirstTurn = false;
							return true;
						}
						else
						{
							return false;
						}
					}
					else if (location.first == _letterLocation - 1 || location.first == _letterLocation + 1)//if diagnol
					{
						if (_board._board[location.second][location.first] != NULL && _board._board[location.second][location.first]->getColor() != true)
						{
							isFirstTurn = false;
							return true;
						}
						else
						{
							return false;
						}
					}
					else
					{
						return false;
					}

				}
				else
				{
					return false;
				}
			}
		}
		else//not first turn of pawn
		{
			if (location.second == (_numberLocation - 1))
			{
				if (location.first == _letterLocation)
				{
					if (_board._board[_numberLocation - 1][_letterLocation] == NULL)
					{
						isFirstTurn = false;
						return true;
					}
					else
					{
						return false;
					}
				}
				else if (location.first == _letterLocation - 1 || location.first == _letterLocation + 1)
				{
					if (_board._board[location.second][location.first] != NULL && _board._board[location.second][location.first]->getColor() != true)
					{
						isFirstTurn = false;
						return true;
					}
					else
					{
						return false;
					}

				}
				else
				{
					return false;
				}

			}
			else
			{
				return false;
			}
		}
	}
	return false;
}