#pragma once
#include "Piece.h"
#include <iostream>

using std::string;

class Pawn : public Piece
{
public:
	Pawn(const bool color, const string& loc, Board& board);//constructor
	virtual bool isMovable(const string& newLocation);//checks if pawn can move to a certain location

private:
	bool isFirstTurn;//whether its the pawns first turn or not
};