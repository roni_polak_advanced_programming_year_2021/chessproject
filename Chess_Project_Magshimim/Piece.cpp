#include "Piece.h"
#include "Rook.h"
#include "Board.h"
#include "Pawn.h"
#include "Queen.h"
#include "Bishop.h"
#include "Knight.h"
#include "King.h"

#define LOWERCASE_ASCII 32
#define GET_LETTER_ASCII 97
#define GET_NUM_ASCII 49
#define BOARD_RESET 7

Piece::Piece(char type, const string& location, const bool color, Board& board) : _location(location), _color(color), _board(board)
{
	if (!_color)//if black
	{
		_type = type + LOWERCASE_ASCII;//make lowercase
	}
	else
	{
		_type = type;
	}
	setLocation();//set indexes of location in vo
}

void Piece::setLocation()
{
	_letterLocation = 0 + int(_location[0]) - 97;
	_numberLocation = 7 - (int(_location[1]) - 49);
}

int Piece::getMoveId(const string& turn)
{
	int color = this->getColor();
	string enemyKingLocation;
	string myKingLocation;
	bool checkIfMovable = 0;
	string tempLoc;
	std::string endLocation = "";

	endLocation += turn[0];
	endLocation += turn[1];

	pair<int, int> location = getBoardLocation(endLocation);

	if (_board._board[location.second][location.first] != NULL)
	{
		if (_board._board[location.second][location.first]->getColor() == color)
		{
			return 3;
		}
	}

	checkIfMovable = isMovable(endLocation);

	//gets the kings locations according to the piece's color
	if (color)
	{
		enemyKingLocation = _board._blackKingLocation;
		myKingLocation = _board._whiteKingLocation;
	}
	else
	{
		enemyKingLocation = _board._whiteKingLocation;
		myKingLocation = _board._blackKingLocation;
	}


	if (!checkIfMovable)//invalid movement
	{
		return 6;
	}
	if (_location.compare(endLocation)==0)//same location
	{
		return 7;
	}


	if (checkIfMovable)//valid move
	{
		tempLoc = _location;//saves location
		Piece* p = _board._board[7 - (int(endLocation[1]) - 49)][int(endLocation[0]) - 97];//saves piece in the wanted location

		_board.move(_location, endLocation);//moves the piece
		_location = endLocation;//change piece location
		setLocation();

		if (isCheck(myKingLocation))//there is check for the player's king
		{
			//move back
			_board.move(_location, tempLoc);
			_location = tempLoc;//change piece location
			setLocation();
			_board._board[7 - (int(endLocation[1]) - 49)][int(endLocation[0]) - 97] = p;

			//checnges king's location if needed
			if (_type == 'K')
			{
				_board._whiteKingLocation = _location;

			}
			else if(_type == 'k')
			{
				_board._blackKingLocation = _location;
			}
			return 4;
		}
		if (isCheck(enemyKingLocation))//there is check for the oponent player
		{
			return 1;
		}
	}

	return 0;//no check but valid move

}

//checks if check on king
bool Piece::isCheck(const string& kingLocation)
{
	pair<int, int> location = getBoardLocation(kingLocation);

	for (int i = 0; i < 8; i++)//goes through the board
	{
		for (int j = 0; j < 8; j++)
		{
			if (_board._board[j][i] != NULL)//if there is a piece in the location
			{
				if (_board._board[location.second][location.first]->getColor() != _board._board[j][i]->getColor())//oponent piece
				{
					if (_board._board[j][i]->isMovable(kingLocation))//can move to eat teh king
					{
						return true;//there is check
					}
				}
			}
		}
	}
	return false;
}


pair<int, int> Piece::getBoardLocation(string loc)
{
	pair<int, int> location;
	location.first = int(loc[0]) - GET_LETTER_ASCII;
	location.second = BOARD_RESET - (int(loc[1]) - GET_NUM_ASCII);

	return location;
}

char Piece::getType() const
{
	return _type;
}
string Piece::getLocation()const
{
	return _location;
}
bool Piece::getColor()const
{
	return _color;
}