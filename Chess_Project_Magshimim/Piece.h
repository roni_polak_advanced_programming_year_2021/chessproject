#pragma once

#include <string>
#include <iostream>

#include "Board.h"
	
using namespace std;

class Board;
class Piece
{
public:
	Piece(char type, const string& location, const bool color, Board& board);//constructor

	virtual int getMoveId(const string& turn);//returns the movements id (if valid or not)
	virtual bool isCheck(const string& kingLocation);//checks if there is check on a location
	virtual pair<int, int> getBoardLocation(string loc);//gets location and returns the indexes of the location in the board
	
	char getType() const;//returns the piece's type
	string getLocation()const;//returns the piece's location
	bool getColor()const;//returns the piece's color
	void setLocation();//sets the indexes of the location un the board

	virtual bool isMovable(const string& newLocation) = 0;//pure virtual function - checks if piece can move to a certain location on the board

protected:
	char _type;
	string _location;
	bool _color;
	Board& _board;//not owned
	int _letterLocation;
	int _numberLocation;
};

