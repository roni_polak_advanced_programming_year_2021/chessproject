#include "Queen.h"

//inherits from both bishop and rook
Queen::Queen(const bool color, const string& loc, Board& board) : Rook(color, loc, board), Bishop(color, loc, board), Piece('Q', loc, color, board)
{

}

bool Queen::isMovable(const string& newLocation)
{
	return Rook::isMovable(newLocation) || Bishop::isMovable(newLocation);//check if can move as either piece
}