#pragma once
#include "Piece.h"
#include "Rook.h"
#include "Bishop.h"
#include <iostream>
using std::string;


class Queen : public Rook, public Bishop
{
public:
	Queen(const bool color, const string& loc, Board& board);//constructor
	virtual bool isMovable(const string& newLocation) override;// checks if queen can move to a certain location
};