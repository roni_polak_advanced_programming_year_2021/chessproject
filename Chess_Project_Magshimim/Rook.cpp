#include "Rook.h"

Rook::Rook(const bool color, const string& loc, Board& board) : Piece('R', loc, color, board)
{

}

bool Rook::isMovable(const string& newLocation)
{
	bool error = false;

	pair<int, int> location = getBoardLocation(newLocation);

	if (_letterLocation == location.first)//moves vertical
	{
		if (_numberLocation < location.second)//moves down
		{
			for (int i = location.second - 1; i > _numberLocation; i--)//goes through the pieces between
			{
				if (_board._board[i][location.first] != NULL)//if there is a piece
				{
					error = true;
					break;
				}
			}
		}
		if (_numberLocation > location.second)//moves up 
		{
			for (int i = location.second + 1; i < _numberLocation; i++)//goes through the pieces between
			{
				if (_board._board[i][location.first] != NULL)//if there is a piece
				{
					error = true;
					break;
				}
			}
		}
	}
	else if (_numberLocation == location.second)//moves horizontal
	{
		if (_letterLocation < location.first)//moves right
		{
			for (int i = location.first - 1; i > _letterLocation; i--)//goes through the pieces between
			{
				if (_board._board[location.second][i] != NULL)//if there is a piece
				{
					error = true;
					break;
				}
			}
		}
		if (_letterLocation > location.first)//moves right
		{
			for (int i = location.first + 1; i < _letterLocation; i++)//goes through the pieces between
			{
				if (_board._board[location.second][i] != NULL)//if there is a piece
				{
					error = true;
					break;
				}
			}
		}

	}
	else
	{
		error = true;
	}

	return !error;
}