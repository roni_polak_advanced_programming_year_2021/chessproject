#pragma once
#include "Piece.h"
#include <iostream>

using std::string;

class Rook : virtual public Piece
{
public:
	Rook(const bool color, const string& loc, Board& board);//rook
	virtual bool isMovable(const string& newLocation);//checks if rook can move to a certain location
};