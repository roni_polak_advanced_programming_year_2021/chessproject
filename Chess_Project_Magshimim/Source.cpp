/*
This file servers as an example of how to use Pipe.h file.
It is recommended to use the following code in your project,
in order to read and write information from and to the Backend
*/

#include "Pipe.h"
#include <iostream>
#include <Windows.h>
#include <thread>
#include "Board.h"
using std::cout;
using std::endl;
using std::string;


void main()
{
	ShellExecute(NULL, NULL, "chessGraphics.exe", NULL, NULL, SW_SHOW);
	system("pause");
	Board b = Board();
	int number = 0;
	//b.printBoard();
	std::string idValue = "";
	srand(time_t(NULL));


	Pipe p;
	bool isConnect = p.connect();

	string ans;
	while (!isConnect)
	{
		cout << "cant connect to graphics" << endl;
		cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
		std::cin >> ans;

		if (ans == "0")
		{
			cout << "trying connect again.." << endl;
			Sleep(5000);
			isConnect = p.connect();
		}
		else
		{
			p.close();
			return;
		}
	}

	int turn = 2;
	char msgToGraphics[1024];
	// msgToGraphics should contain the board string accord the protocol
	// YOUR CODE

	strcpy_s(msgToGraphics, "rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR1");
	//"################################################PPPPPPPPRNBKQBNR1"
	cout << msgToGraphics;
	p.sendMessageToGraphics(msgToGraphics);   // send the board string

	// get message from graphics
	string msgFromGraphics = p.getMessageFromGraphics();

	while (msgFromGraphics != "quit")
	{
		// should handle the string the sent from graphics
		// according the protocol. Ex: e2e4           (move e2 to e4)

		// YOUR CODE
		//check if valid
		std::string newLocation = "";
		newLocation += msgFromGraphics[2];
		newLocation += msgFromGraphics[3];
		
		if (b._board[7 - (int(msgFromGraphics[1]) - 49)][0 + int(msgFromGraphics[0]) - 97] != NULL)
		{
			if (turn % 2 != 0)//white
			{
				if (b._board[7 - (int(msgFromGraphics[1]) - 49)][0 + int(msgFromGraphics[0]) - 97]->getColor())
				{
					number = b._board[7 - (int(msgFromGraphics[1]) - 49)][0 + int(msgFromGraphics[0]) - 97]->getMoveId(newLocation);
					if (number == 0 || number == 1)
					{
						turn++;
					}
				}
				else
				{
					number = 2;
				}
			
			}
			else//black
			{
				if (!b._board[7 - (int(msgFromGraphics[1]) - 49)][0 + int(msgFromGraphics[0]) - 97]->getColor())
				{
					number = b._board[7 - (int(msgFromGraphics[1]) - 49)][0 + int(msgFromGraphics[0]) - 97]->getMoveId(newLocation);
					if (number == 0 || number == 1)
					{
						turn++;
					}
				}
				else
				{
					number = 2;
				}
			}
			
			idValue = to_string(number);
		}
		else
		{
			idValue = "2";
		}



		//if valid:
		//get board string and send to front end
		strcpy_s(msgToGraphics, idValue.length() + 1, idValue.c_str()); // msgToGraphics should contain the result of the operation

		/******* JUST FOR EREZ DEBUGGING ******/
		//int r = rand() % 10; // just for debugging......
		//msgToGraphics[0] = (char)(3 + '0');
		//	msgToGraphics[1] = 0;
		/******* JUST FOR EREZ DEBUGGING ******/


		// return result to graphics		
		p.sendMessageToGraphics(msgToGraphics);

		// get message from graphics
		msgFromGraphics = p.getMessageFromGraphics();
	}

	p.close();
}